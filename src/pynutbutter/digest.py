#!/usr/bin/env python

import json
from typing import Any, List, Union
from measurement.measures import Weight
from requests import Response
from operator import attrgetter
from measurement.utils import guess
from pathlib import Path
from .fdc_enums import fdc_data_type
from .fdc_data_objects import (
  Food,
  FoodAttribute,
  FoodAttributeType,
  FoodCategory,
  FoodComponent,
  FoodPortion,
  MeasureUnit,
  Nutrient,
  NutrientConversionFactor,
  SurveyInputFood,
  WweiaFoodCategory
)
from .pynutbutter import debugging, logg, config, daily_values


def ensure_val(dictionary: dict, key: str, default: Any) -> Any:
  """Check to see if a key is in a dictionary. If it's not, we return the
     desired default

    Args:
      dictionary: the dict to search through
      key: the key to look for
      default: A default to apply if the key is missing.
  """
  if key in dictionary.keys():
    return dictionary[key]
  else:
    return default


################################################################################
# TODO: Add support for more kwargs
################################################################################


def chew(r: Response, **kwargs) -> list[Food]:
  """Takes the JSON response from FDC and turns it a list of 1 or more food
      objects.

  Args:
      r (Response): The response from the FDC API query
      **kwargs: A dictonary of config items passed to follow-on functions.
                (only supports 'max_items' at this time)

  Returns:
      list[fdcdo.Food]: A list of one or more Food objects.
  """
  foods: List[Food] = []

  if debugging():
    logg("debug", f"Chewing JSON response(s) with kwargs: {kwargs}.")

  if 'max_items' in kwargs.keys() and debugging():
    logg("debug", f"Client asked for a max of {kwargs['max_items']} results.")

  if r:
    cache_id = r.cache_key  # type: ignore
    if hasattr(r, 'size'):  # type: ignore
      size = f"{round(r.size/1024)} kb"  # type: ignore
      logg("info", f"Using cached result, saving {size}. (ID: {cache_id})")
    else:
      logg("info", f"Caching result. (ID: {cache_id})")

    try:
      json_response = json.loads(r.content.decode('utf-8'))
    except Exception as e:
      logg("error", f"Unable to decode response: {e}")

    num_results = json_response['totalHits']

    if debugging():
      logg("debug", f"Received {num_results} results.")

    for food in json_response['foods']:
      current_food = Food(
        fdc_id=food['fdcId'],
        description=food['description'],
        data_type=fdc_data_type(food['dataType'])
      )

      if 'foodNutrients' in food:
        dco = config.getint('DEFAULT', 'daily_cal')
        nutrients: List[Nutrient] = []

        for nutrient in food['foodNutrients']:
          if 'derivationCode' in nutrient:
            nutr = Nutrient(
              number=nutrient['nutrientNumber'],
              name=nutrient['nutrientName'],
              amount=float(nutrient['value']),
              unit=nutrient['unitName'].lower(),
              derivation_code=nutrient['derivationCode'],
              derivation_description=nutrient['derivationDescription']
            )

          else:
            nutr = Nutrient(
              id=nutrient['id'],
              amount=float(nutrient['nutrient']['median']),
              number=nutrient['number'],
              name=nutrient['name'],
              rank=nutrient['rank'],
              unit=nutrient['unit'].lower()
            )
          nutr.set_dv(nutr.amount, nutr.unit)
          nutrients.append(nutr)
        nutrients = sorted(nutrients, key=attrgetter('number'))
        current_food.add_nutrients(nutrients)

      if 'foodUpdateLog' in food:
        logs = []
        for log in food['foodUpdateLog']:
          logs.append(log)
        current_food.add_update_log(logs)

      if 'servingSize' in food or 'foodPortions' in food:
        portions: List[FoodPortion] = []
        if 'servingSize' in food:
          serving_text = None
          if 'householdServingFullText' in food:
            serving_text = food['householdServingFullText']
          portions = [
            FoodPortion(
              amount=float(food['servingSize']),
              serving_size=serving_text,
              measure_unit=MeasureUnit(abbr=food['servingSizeUnit'])
            )
          ]
        else:
          for portion in food['foodPortions']:
            portions.append(
              FoodPortion(
                id=portion['id'],
                amount=portion['amount'],
                data_points=portion['dataPoints'],
                weight=portion['gramWeight'],
                year_acquired=portion['minYearAcquired'],
                description=portion['portionDescription'],
                sort_num=portion['sequenceNumber'],
                measure_unit=MeasureUnit(
                  id=portion['measureUnit']['id'],
                  name=portion['measureUnit']['name'],
                  abbr=portion['measureUnit']['abbr']
                )
              )
            )

        current_food.add_portions(portions)

      if 'labelNutrients' in food:
        current_food.add_label_nutrients(json.loads(food['labelNutrients']))

      if 'foodComponents' in food:
        components: List[FoodComponent] = []
        for component in food['foodComponents']:
          components.append(
            FoodComponent(
              id=component['id'],
              name=component['name'],
              weight=component['gramWeight'],
              refuse=component['isRefuse'],
              data_points=component['dataPoints'],
              year_acquired=component['minYearAcquired'],
              percent_weight=component['percentWeight']
            )
          )
        current_food.add_components(components)

      if 'nutrientConversionFactors' in food:
        conversion_factors: List[NutrientConversionFactor] = []
        for factor in food['nutrientConverstionFactors']:
          conversion_factors.append(
            NutrientConversionFactor(
              type=factor['type'], value=factor['value']
            )
          )
        current_food.add_nutrient_conversion_factors(conversion_factors)

      if 'inputFoods' in food:
        input_foods: List[SurveyInputFood] = []
        for input in food['inputFoods']:
          input_foods.append(
            SurveyInputFood(
              id=input['id'],
              amount=input['amount'],
              code=input['ingredientCode'],
              description=input['ingredientDescription'],
              weight=input['ingredientWeight'],
              portion_code=input['portionCode'],
              portion_description=input['portionDescription'],
              sequence=input['sequenceNumber'],
              survey_flag=input['surveyFlag'],
              unit=input['unit']
            )
          )
        current_food.add_input_foods(input_foods)

      if 'wweiaFoodCategory' in food:
        current_food.add_wweia(
          WweiaFoodCategory(
            code=food['wweiaFoodCategory']['wweiaFoodCategoryCode'],
            description=food['wweiaFoodCategory']
            ['wweiaFoodCategoryDescription']
          )
        )

      if 'foodCategory' in food or 'foodAttribute' in food:
        if 'foodCategory' in food:
          if 'id' in food['foodCategory']:
            attributes: List[FoodCategory]
            attributes.append(
              FoodCategory(
                id=food['foodCategory']['id'],
                code=food['foodCategory']['code'],
                description=food['foodCategory']['description']
              )
            )
          else:
            attributes = food['foodCategory']
        else:
          attributes = List[FoodAttribute]
          for attr in food['foodAttributes']:
            attributes.append(
              FoodAttribute(
                id=attr['id'],
                sequence=attr['sequenceNumber'],
                value=attr['value'],
                attribute_type=FoodAttributeType(
                  id=attr['foodAttributeType']['id'],
                  name=attr['foodAttributeType']['name'],
                  description=attr['foodAttributeType']['description']
                )
              )
            )

      if 'availableDate' in food:
        current_food.add_available_date(food['availableDate'])

      if 'brandOwner' in food:
        if 'brandName' in food:
          current_food.add_brand(
            f"{food['brandOwner'].title()}'s {food['brandName'].title()}"
          )
        else:
          current_food.add_brand(food['brandOwner'].title())

      if 'foodClass' in food:
        current_food.add_food_class(food['foodClass'])

      if 'foodCode' in food:
        current_food.add_food_code(food['foodCode'])

      if 'dataSource' in food:
        current_food.add_data_source(food['dataSource'])

      if 'foodNote' in food:
        current_food.add_footnotes(food['footNote'])

      if 'isHistoricalReference' in food:
        current_food.flag_historical(food['isHistoricalReference'])

      if 'householdServingFullText' in food:
        current_food.add_household_serving_size(
          food['householdServingFullText']
        )

      if 'ingredients' in food:
        current_food.add_ingredients(food['ingredients'].title())

      if 'ndbNumber' in food:
        current_food.add_ndb_id(food['ndbNumber'])

      if 'publicationDate' in food or 'publishedDate':
        if 'publicationDate' in food:
          current_food.add_pub_date(food['publicationDate'])
        else:
          current_food.add_pub_date(food['publishedDate'])

      if 'scientificName' in food:
        current_food.add_scientific_name(food['scientificName'])

      if 'servingSize' in food:
        current_food.add_serving_size(food['servingSize'])

      if 'servingSizeUnit' in food:
        current_food.add_serving_unit(food['servingSizeUnit'])

      if 'startDate' in food:
        current_food.add_survey_start(food['startDate'])
        current_food.add_survey_end(food['endDate'])

      if 'gtinUpc' in food:
        current_food.add_upc(food['gtinUpc'])

      foods.append(current_food)

    return foods


def dv(nutrient: str, amount: float, measure: str) -> Union[float, None]:
  """ Return the recommended daily value percentage of a nutrient

    Args:
      nutrient: The nutrient in question
      amount: The amount of nutrients provided by this food item
      measure: The measurement of nutrients provided by this food item

    Returns:
      Float if the nutrient is a daily value nutrient
      None if the nutrient is not a daily value nutrient
  """
  if debugging():
    logg("debug", f"Lookingn for DV nutrient. {nutrient}.")

  nutrient = nutrient.replace(' ', '_').strip
  nutrient = nutrient.upper()

  if nutrient in dv_nutrients:
    if nutrients[nutrient] < 0.01:
      meas = Weight(measure=amount).mcg
    elif nutrients[nutrient] > 1000:
      meas = Weight(measure=amount).g
    else:
      meas = Weight(measure=amount).mg

    return meas / nutrients[nutrient]
  else:
    if debugging():
      logg("debug", f"Not a DV nutrient. {nutrient}({amount}{measure})!")
    return None
