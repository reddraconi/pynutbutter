#!/usr/bin/env python

from sys import exit

import requests
from requests.sessions import HTTPAdapter
from urllib3.util.retry import Retry  # type: ignore

from .digest import chew
from .fdc_data_objects import Food
from .fdc_enums import (
  fdc_data_format,
  fdc_data_type,
  fdc_request,
  fdc_sort_by,
  fdc_sort_order,
  fdc_nutrients
)
from .pynutbutter import (
  apikey,
  apiemail,
  debugging,
  backoff,
  logg,
  max_retries,
  transaction_timeout,
  __version__
)


def fail_safely() -> None:
  """ Safely closes HTTP connections if something reported a problem."""
  logg(
    "exception",
    "A critical exception has occured. Bailing and closing connections."
  )
  if http:
    http.close()
  exit(1)


# Call 'raise_for_response' on every request object for safety
raise_status_hook = lambda response, *args, **kwargs: \
  handle_rest_response(response)

# Set all HTTP-based requests to come from 'base_url', to be handled
# per-request, and to enforce a connection timeout and retires with exponential
# backoff.
retry_config = Retry(
  total=max_retries(),
  status_forcelist=[429, 500, 502, 503, 504],
  allowed_methods=['HEAD', 'GET', 'OPTIONS'],
  backoff_factor=backoff(),
)
http = requests.Session()
http.hooks["response"] = [raise_status_hook]
http.mount("http://", HTTPAdapter(max_retries=retry_config))
http.mount("https://", HTTPAdapter(max_retries=retry_config))
http.headers.update({
  'User-Agent':
    f"Pynutbutter Python Module v{__version__} "
    f"[https://pypi.org/project/pynubutter]",
  'Content-Type': 'application/json',
  'Accept': 'application/json',
  'From': f"{apiemail()}",
})


def handle_rest_response(response: requests.Response) -> None:
  """ Logs according to the REST response from the server
      Fails safely if the rest response is: too many redirects, HTTP error,
      connection error, connection timeout, or if there was a problem with the
      query itself.
  """
  if debugging():
    logg("debug", "REST request sent and received. Processing.")
  try:
    response.raise_for_status()
  except requests.exceptions.TooManyRedirects as redir_err:
    logg("error", f"Too many redirects! Check the URL. {redir_err}")
    fail_safely()
  except requests.exceptions.HTTPError as http_error:
    logg("error", f"An HTTP Error occured in the request: {http_error}")
    fail_safely()
  except requests.exceptions.ConnectionError as conn_error:
    logg("error", f"A connection error occured: {conn_error}")
  except requests.exceptions.Timeout as timeout_err:
    logg("error", f"The connection timed out: {timeout_err}")
  except requests.exceptions.RequestException as req_err:
    logg("error", f"The server reported an error in the request: {req_err}")
    fail_safely()
  except Exception as e:
    logg("error", f"An unknown error occured: {e}")
    fail_safely()


def query(**kwargs) -> list[Food]:
  """Flexible querying mechanism. Figures out what to do based on supplied
     arguments. Sets defaults to missing kwargs and drops any unknown kwargs.

  Args:
    query (str): Search for one or more foods based on a string. Cannot be
      used with 'fdcid'
    fdcid list(int): One or more FDC Food IDs. Cannot be used with 'query'
    format (food_data_enums.food_data_format): Controls the output format of
      the FDC API request. Cannot be used with 'query'.
    nutrients list(food_data_enums.fdc_nutrients): one or more nutrients to
      query for. 25 nutrients, max. Anything more will be silently truncated.
      Cannot be used with 'query'
    data_type list(fdc_enums.fdc_data_type): One or more databases to search
      through. Can only be used with 'query'.
    max_items (int): Contols the number of results. Defaults to 25.
      Can only be used with 'query'.
    page_number (int): Controls the current page of results. Can only be used
      with 'query'.
    sort_by (fdc_enums.fdc_sort_by): Controls how results are sorted. Can only
      be used with 'query'.
    sort_order (fdc_enums.fdc_sort_order): Controls sorted results are sorted.
      Can only be used with 'query'.
    brand (str): Used to filter results down to a sigle brand. Only use if
      'data_type' contains 'fdc_enums.fdc_data_type.BRANDED'.

  Returns:
    list[Food]: A list of one or more Food objects.
  """

  if debugging():
    logg("debug", f"Query received with kwargs: {kwargs}.")

  args = {
    'query': None,
    'fdcid': [],
    'format': fdc_data_format.ABRIDGED.value,
    'nutrients': [],
    'data_type': fdc_data_type.FOUNDATION.value,
    'max_items': 25,
    'sort_by': fdc_sort_by.KEYWORD.value,
    'sort_order': fdc_sort_order.ASC.value,
    'brand': None,
  }

  if 'query' in kwargs.keys():
    args['query'] = kwargs['query']

  if 'fdcid' in kwargs.keys():
    args['fdcid'] = kwargs['fdcid']

  if 'format' in kwargs.keys():
    if kwargs['format'] in fdc_data_format:
      args['format'] = kwargs['format']
    else:
      logg(
        "warning",
        f"Requsted query data format was not recognized: {kwargs['format']}. "
        f"Defaulting to abridged format."
      )

  if 'nutrients' in kwargs.keys():
    args['nutrients'] = kwargs['nutrients']

  if 'data_type' in kwargs.keys():
    args['data_type'] = kwargs['data_type']

  # The FDC API supports 1-25 keys at a time.
  # TODO: Split up requests into chunks of 25 and append to results.

  if 'max_items' in kwargs.keys():
    if kwargs['max_items'] > 50:
      kwargs['max_items'] = 50
    if kwargs['max_items'] < 1:
      kwargs['max_items'] = 1
    args['max_items'] = kwargs['max_items']

  if 'sort_by' in kwargs.keys():
    if kwargs['sort_by'] in fdc_sort_by:
      args['sort_by'] = kwargs['sort_by']
    else:
      logg(
        "warning",
        f"Requsted query sort type not recognized: {kwargs['sort_by']}. "
        f"Defaulting to sort by keyword."
      )

  if 'sort_order' in kwargs.keys():
    if kwargs['sort_order'] in fdc_sort_order:
      args['sort_order'] = kwargs['sort_order']
    else:
      logg(
        "warning",
        f"Requested sort order was not recognized: {kwargs['sort_order']}. "
        f"Defaulting to sort ascending."
      )

  if 'brand' in kwargs.keys():
    args['brand'] = kwargs['brand']

  # Handle query by string fist.

  if 'query' in kwargs.keys():
    if debugging():
      logg("debug", "Found query keyword. Ignoring query-incompatible options.")

    logg("info", f"Querying for '{args['query']}'")

    return search_foods(
      query=args['query'],
      data_type=args['data_type'],
      max_items=args['max_items'],
      sort_by=args['sort_by'],
      sort_order=args['sort_order'],
      brand=args['brand'],
      format=args['format']
    )
  else:
    if (args['fdcid'].length > 1):
      logg("info", f"Searching for the following id(s): {args['fdcid']}")
    else:
      logg("info", f"Searching for the following id: {args['fdcid']}")

    return list_foods(
      fdcIds=args['fdcid'], nutrients=args['nutrients'], format=args['format']
    )


def list_foods(
  fdcIds: list[int], nutrients: list[fdc_nutrients], format: fdc_data_format
) -> list[Food]:
  """Queries the FDC API for the provided list of IDs.

  Args:
    fdcIds (list[int]): One or more FDC IDs that we want info on. Max of 20
      per request. If more than 20 are submitted, this request will be broken
      up into chunks.
    nutrients (list[int], optional): List of Nutrient IDs we want to return
      Defaults to []. 25 nutrients can be listed, max. All others will be
      silently truncated.
    format (fdc_data_format, optional): Data format returned.
      Defaults to fdc_data_format.ABRIDGED.

  Returns:
    A list of Food objects
  """
  foods = []
  endpoint = endpoint = fdc_request.LIST.value  #type:ignore
  request_parameters = {'API_KEY': apikey(), 'format': format}

  if debugging():
    logg("debug", f"Sending API request to {endpoint}")

  # Truncate data, if necessary. The API only supports 25 nutrients in a list,
  # max.
  if len(nutrients) > 0:
    if len(nutrients) > 25:
      nutrients = nutrients[:25]
    request_parameters['nutrients'] = nutrients

  # The API only supports 20 FDC IDs at a time. We have to split up requests to
  # fit, if necessary.
  if len(fdcIds) > 20:
    for group in [fdcIds[i:i + 20] for i in range(0, len(fdcIds), 20)]:
      request_parameters['fdcIds'] = group

      foods.extend(
        chew(
          http.get(
            url=endpoint,
            params=request_parameters,  #type:ignore
            timeout=transaction_timeout()
          )
        )
      )
    return foods
  else:
    return chew(
      http.get(
        url=endpoint,
        params=request_parameters,  #type:ignore
        timeout=transaction_timeout()
      )
    )


def search_foods(
  query: str,
  data_type: list[fdc_data_type],
  max_items: int,
  format: fdc_data_format,
  sort_by: fdc_sort_by,
  sort_order: fdc_sort_order,
  brand: str
) -> list[Food]:
  """Returns an abridged list of foods from the FDC API

  Args:
    query (str): The food you're looking for.
    data_type (list[fdc_data_type], optional): The database(s) you want to
    query. Defaults to [fdc_data_type.FOUNDATION] but can be a list of more
    than one fdc_data_type.
    max_items (int, optional): The number of results to return.
      Defaults to 25.
    sort_by (fdc_sort_by, optional): How the results should be sorted.
      Defaults to fdc_sort_by.KEYWORD.
    sort_order (fdc_sort_order, optional): How the sorted results sould be
    sorted. Defaults to fdc_sort_order.ASC.
    brand (str, optional): If searching the BRANDED database, this will be
      used to further filter the results down to a single brand.

  Returns:
    A list of Food objects
  """

  endpoint = fdc_request.SEARCH.value  #type:ignore

  request_parameters = {
    'api_key': apikey(),
    'query': query,
    'dataType': data_type,
    'sortBy': sort_by,
    'sortOrder': sort_order,
    'pageSize': max_items,
    'format': format
  }

  if brand is not None and fdc_data_format.BRANDED.value not in data_type:  #type:ignore
    logg(
      "warning",
      "Brand was supplied, but we weren't told to look in the BRANDED database!"
      " Ignoring brand configuration."
    )
  elif brand is not None and fdc_data_format.BRANDED.value in data_type:  #type:ignore
    request_parameters['brandOwner'] = brand

  if debugging():
    logg(
      "debug",
      f"Sending API request to {endpoint} using parameters {request_parameters}"
    )

  return chew(
    http.get(
      url=endpoint,
      params=request_parameters,  #type:ignore
      timeout=transaction_timeout()
    ),
    max_items=max_items
  )
