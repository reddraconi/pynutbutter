import requests
from .digest import chew as chew
from .fdc_data_objects import Food as Food
from .fdc_enums import (
  fdc_data_format as fdc_data_format,
  fdc_data_type as fdc_data_type,
  fdc_nutrients as fdc_nutrients,
  fdc_request as fdc_request,
  fdc_sort_by as fdc_sort_by,
  fdc_sort_order as fdc_sort_order
)
from .pynutbutter import apiemail as apiemail, apikey as apikey, backoff as backoff, debugging as debugging, logg as logg, max_retries as max_retries, transaction_timeout as transaction_timeout
from datetime import time as time
from requests_toolbelt import sessions as sessions
from typing import Any


def fail_safely() -> None:
  ...


raise_status_hook: Any
retry_config: Any
http: Any


def handle_rest_response(response: requests.Response) -> None:
  ...


def query(**kwargs) -> list[Food]:
  ...


def list_foods(
  fdcIds: list[int], nutrients: list[fdc_nutrients], format: fdc_data_format
) -> list[Food]:
  ...


def search_foods(
  query: str,
  data_type: list[fdc_data_type],
  total_hits: int,
  format: fdc_data_format,
  sort_by: fdc_sort_by,
  sort_order: fdc_sort_order,
  brand: str
) -> list[Food]:
  ...
