from _typeshed import Incomplete
from aenum import Enum


class fdc_data_type(str, Enum):
  BRANDED: str
  FOUNDATION: str
  SURVEY: str
  LEGACY: str


class fdc_sort_by(Enum):
  KEYWORD: str
  DESCRIPTION: str
  ID: str
  PUBDATE: str


class fdc_sort_order(Enum):
  ASC: str
  DESC: str


class fdc_data_format(Enum):
  ABRIDGED: str
  FULL: str


class fdc_request(Enum):
  SEARCH: str
  LIST: str


class fdc_nutrients(Enum):
  ALA: Incomplete
  BIOTIN: Incomplete
  CAFFIENE: Incomplete
  CALCIUM: Incomplete
  CALORIE: Incomplete
  CARBOHYDRATE: Incomplete
  CHOLESTEROL: Incomplete
  CHOLINE: Incomplete
  COPPER: Incomplete
  DHA: Incomplete
  EPA: Incomplete
  ENERGY: Incomplete
  FAT: Incomplete
  FAT_TOTAL: Incomplete
  FIBER: Incomplete
  FLUORIDE: Incomplete
  FOLATE: Incomplete
  IODINE: Incomplete
  IRON: Incomplete
  MANGANESE: Incomplete
  MANGESIUM: Incomplete
  MONOUNSATURATED_FAT: Incomplete
  NIACIN: Incomplete
  PANTOTHENIC_ACID: Incomplete
  PHOSPHORUS: Incomplete
  PHYLLOQUINONE: Incomplete
  POLYUNSATURATED_FAT: Incomplete
  POTASSIUM: Incomplete
  PROTEIN: Incomplete
  RIBOFLAVIN: Incomplete
  SATURATED_FAT: Incomplete
  SELENIUM: Incomplete
  SODIUM: Incomplete
  SUGAR: Incomplete
  SUGARS: Incomplete
  THEOBROMINE: Incomplete
  THIAMIN: Incomplete
  TRANS_FAT: Incomplete
  VIT_A: Incomplete
  VIT_B1: Incomplete
  VIT_B12: Incomplete
  VIT_B2: Incomplete
  VIT_B3: Incomplete
  VIT_B5: Incomplete
  VIT_B6: Incomplete
  VIT_B9: Incomplete
  VIT_C: Incomplete
  VIT_D: Incomplete
  VIT_E: Incomplete
  VIT_K: Incomplete
  WATER: Incomplete
  ZINC: Incomplete


class fdc_dv(Enum):
  CHLORIDE: Incomplete
  CHROMIUM: Incomplete
  MOLYBEDNIUM: Incomplete
  VIT_D_IU: Incomplete
  VIT_E_IU: Incomplete
