from importlib.metadata import version

# Set version number from installed packages.
__version__ = version('pynutbutter')

if __name__ == "__main__":
  print(f"Pynutbutter v{__version__}")
  exit(0)
