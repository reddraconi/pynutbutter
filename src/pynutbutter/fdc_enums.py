#!/usr/bin/env python

from aenum import Enum


class fdc_data_type(str, Enum):
  BRANDED = "Branded"
  FOUNDATION = "Foundation"
  SURVEY = "Survey (FNDDS)"
  LEGACY = "SR Legacy"

  @classmethod
  def _missing_name_(cls, name):
    for member in cls:
      if member.name.lower() == name.lower():
        return member


class fdc_sort_by(Enum):
  KEYWORD = "dataType.keyword"
  DESCRIPTION = "lowercaseDescription.keyword"
  ID = "fdcId"
  PUBDATE = "publishedDate"

  @classmethod
  def _missing_name_(cls, name):
    for member in cls:
      if member.name.lower() == name.lower():
        return member


class fdc_sort_order(Enum):
  ASC = "asc"
  DESC = "desc"

  @classmethod
  def _missing_name_(cls, name):
    for member in cls:
      if member.name.lower() == name.lower():
        return member


class fdc_data_format(Enum):
  ABRIDGED = "abridged"
  FULL = "full"

  @classmethod
  def _missing_name_(cls, name):
    for member in cls:
      if member.name.lower() == name.lower():
        return member


class fdc_request(Enum):
  SEARCH = "https://api.nal.usda.gov/fdc/v1/foods/search"
  LIST = "https://api.nal.usda.gov/fdc/v1/foods"


class fdc_nutrients(Enum):
  ALA = 851.0
  BIOTIN = 999.0
  CAFFIENE = 262.0
  CALCIUM = 301.0
  CALORIE = 208.0
  CARBOHYDRATE = 205.0
  CHOLESTEROL = 601.0
  CHOLINE = 421.0
  COPPER = 312.0
  DHA = 621.0
  EPA = 629.0
  ENERGY = 208.0
  FAT = 204.0
  FAT_TOTAL = 298.0
  FIBER = 291.0
  FLUORIDE = 313.0
  FOLATE = 417.0
  IODINE = 314.0
  IRON = 303.0
  MANGANESE = 315.0
  MANGESIUM = 304.0
  MONOUNSATURATED_FAT = 645.0
  NIACIN = 406.0
  PANTOTHENIC_ACID = 410.0
  PHOSPHORUS = 305.0
  PHYLLOQUINONE = 430.0
  POLYUNSATURATED_FAT = 646.0
  POTASSIUM = 306.0
  PROTEIN = 203.0
  RIBOFLAVIN = 405.0
  SATURATED_FAT = 606.0
  SELENIUM = 317.0
  SODIUM = 307.0
  SUGAR = 269.3
  SUGARS = 269.0
  THEOBROMINE = 263.0
  THIAMIN = 404.0
  TRANS_FAT = 605.0
  VIT_A = 318.0
  VIT_B1 = 404.0
  VIT_B12 = 418.0
  VIT_B2 = 405.0
  VIT_B3 = 406.0
  VIT_B5 = 410.0
  VIT_B6 = 415.0
  VIT_B9 = 417.0
  VIT_C = 401.0
  VIT_D = 324.0
  VIT_E = 323.0
  VIT_K = 430.0
  WATER = 255.0
  ZINC = 309.0
