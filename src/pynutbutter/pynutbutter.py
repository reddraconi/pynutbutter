#!/usr/bin/env python

import logging
import json
import os
import stat
import requests_cache

from configparser import ConfigParser
from datetime import datetime
from email.utils import parseaddr
from importlib.metadata import version
from pathlib import Path
from re import IGNORECASE, match
from sys import exit

from appdirs import (
  user_config_dir,
  user_data_dir,  # type: ignore
  user_log_dir
)

# Set version number from installed package ####################################

__version__ = version('pynutbutter')

# Setup Directories and Data File Paths ########################################

log_dir = Path(user_log_dir("pynutbutter"))
user_cache = Path(user_data_dir("pynutbutter"))
config_file = Path(user_config_dir('pynutbutter'), "config.ini")
cache_file = Path(user_data_dir('pynutbutter'), "pynutbutter.sqlite")
nutrients_file = os.path.join(
  os.path.dirname(__file__), "data", "daily_values.json"
)

debug = False
config = ConfigParser()

# Cache API responses for quicker usage! #######################################
requests_cache.install_cache(cache_file, backend='sqlite', expire="3600")


def load_configuration() -> None:
  """Loads the module configuration

  After validating (or creating) the configuration, logging is configured via
  `setup_logging`.
  """

  requires_writing = False

  ### Config Folders ##########################################################

  # NOTE: We cannot use the logger yet, as that requires the configuration to be
  #       in place, so we just print() and exit() with a non-zero value if any
  #       errors crop up until logging can be initalized.

  config_path = os.path.dirname(Path(config_file))

  if not os.path.isdir(config_path):

    try:
      os.makedirs(config_path)
    except Exception as e:
      print(f"Unable to create directory '{config_path}': {e}")
      exit(1)
  else:
    if not os.path.isfile(os.path.abspath(Path(config_file))):
      print(
        f"ERROR: Configuration file missing. Copy one to '{config_file}'!\n"
        f"See https://gitlab.com/reddraconi/pynutbutter for more info."
      )
      exit(1)

  if not os.path.isdir(log_dir):
    try:
      os.makedirs(log_dir)
    except Exception as e:
      print(f"Unable to create directory {log_dir}: {e}")
      exit(1)

  ### Configuration File and Contents #########################################

  try:
    with Path(config_file).open(mode='r') as config_fp:
      config.readfp(config_fp)
  except Exception as e:
    print(f"Unable to load configuration file from {config_file}: {e}")
    exit(1)

  #### apikey is REQUIRED for pynutbutter to function. Without it, we can't
  #### make calls to the upstream API

  if not config.has_option('DEFAULT', 'apikey') or \
    config.get('DEFAULT', 'apikey') == "<API Key>":
    print(
      f"apikey missing from configuration at {config_file}! "
      f"Visit https://fdc.nal.usda.gov/api-key-signup.html to get one."
    )
    exit(1)

  #### apiemail is REQUIRED for pynutbutter to function. We stuff it into the
  #### API call so the upstream API maintainers can email naughty users

  if not config.has_option('DEFAULT', 'apiemail'):
    print(
      f"apiemail missing from configuration at {config_file}! "
      f"The email you used to sign up for your API key is required!"
    )
    exit(1)
  else:
    if not '@' in parseaddr(config.get('DEFAULT', 'apiemail'))[1] or \
    config.get('DEFAULT', 'apiemail') == "<API email>":
      print(
        f"""The configured apiemail in {config_file} looks incorrect.
        Please correct it and try again."""
      )
      exit(1)

  #### debug is not required, but is stored. This lets the user toggle if
  #### pynutbutter should run in debug mode. This enables much more verbose
  #### output and logging.

  if not config.has_option('DEFAULT', 'debug'):
    requires_writing = True
    config.set('DEFAULT', 'debug', 'false')
  else:
    try:
      config.getboolean('DEFAULT', 'debug')
    except ValueError:
      print(
        f""" Invalid value for 'debug' in {config_file}!  Must be "yes", "no",
        "true", or "false". Defaulting to "false"."""
      )
      requires_writing = True
      config.set('DEFAULT', 'debug', 'false')

  #### log_dir is required. If it's not defined by the user, we create
  #### a path in their XDG USER LOGGING path

  if not config.has_option('DEFAULT', 'log_dir'):
    requires_writing = True
    config.set('DEFAULT', 'log_dir', f"{log_dir}")
  else:
    try:
      config.get('DEFAULT', 'log_dir')
    except ValueError as e:
      print(
        f"""WARNING: Unknown configuration for log_dir in {config_file}!
        Defaulting to {log_dir}"""
      )
      requires_writing = True
      config.set('DEFAULT', 'log_dir', f"{log_dir}")

  if config.get('DEFAULT', 'log_dir') == "":
    print(
      f"""WARNING: Uknown configuration for log_dir in {config_file}!
      Defaulting to '{log_dir}'"""
    )
    requires_writing = True
    config.set('DEFAULT', 'log_dir', f"{log_dir}")

  #### timeout is optional. This controls the overall network timeout for
  #### external API calls. This is set to 5.0 seconds by default.

  if not config.has_option('DEFAULT', 'timeout'):
    requires_writing = True
    config.set('DEFAULT', 'timeout', '5.0')
  else:
    try:
      config.getfloat('DEFAULT', 'timeout')
    except ValueError:
      print(
        f"""Unknown value for timeout in {config_file}.
        Defaulting to 5.0 seconds."""
      )
      requires_writing = True
      config.set('DEFAULT', 'timeout', '5.0')

  #### max_retries is optional. This controls the overall number of times that
  #### an external API call will be made. This is set to 3 by default.

  if not config.has_option('DEFAULT', 'max_retries'):
    requires_writing = True
    config.set('DEFAULT', 'max_retries', '3')
  else:
    try:
      config.getint('DEFAULT', 'max_retries')
    except ValueError:
      print(f"Unknown value for max_retries in {config_file}. Defaulting to 3.")
      requires_writing = True
      config.set('DEFAULT', 'max_retries', '3')

  #### backoff timeout is optional. This controls the number of seconds that
  #### will be placed between attempts of external API calls

  if not config.has_option('DEFAULT', 'backoff'):
    requires_writing = True
    config.set('DEFAULT', 'backoff', '2')
  else:
    try:
      config.getint('DEFAULT', 'backoff')
    except ValueError:
      print(
        f"""Uknown configuration for backoff in {config_file}.
        Defaulting to 2 sec."""
      )
      requires_writing = True
      config.set('DEFAULT', 'backoff', '2')

  #### daily_kcal is optional and allows the user to override the number of
  #### calories they consider to be a day's maximum. This is set to 2000 by
  #### default, which is the current UDSA recommendation.

  if not config.has_option('DEFAULT', 'daily_cal'):
    requires_writing = True
    config.set('DEFAULT', 'daily_cal', '2000')
  else:
    try:
      config.getint('DEFAULT', 'daily_cal')
    except ValueError:
      print(f"Daily caloric intake is invalid. Defaulting to 2000.")
      requires_writing = True
      config.set('DEFAULT', 'daily_cal')

  #### If the configuration file needs to be updated because it's missing
  #### information, the configuration file is updated here.

  if requires_writing:
    try:
      with open(config_file, 'w') as configfile:
        config.write(configfile, space_around_delimiters=True)
      print(f"Configuration saved to '{config_file}'.")
    except Exception as e:
      print(f"WARNING! Unable to save config to {config_file}!: {e}")
      exit(1)

  ## Configure Application Logging #############################################

  setup_logging(config['DEFAULT']['log_dir'], debug=debugging())

  ## Populate Daily Values for Nutrients #######################################

  with open(nutrients_file) as file:
    dv_nutrients = json.load(file)

  if debugging():
    logg("debug", "Populated daily nutritional values.")

  if not dv_nutrients:
    logg("critical", f"Unable to open {nutrients_file}!")
    exit(1)

  logg("info", "pynutbutter has loaded and is ready to process requests.")


class colorized_logging_formatter(logging.Formatter):
  debug = "\x1b[35;20m"  # purple
  informational = "\x1b[38;20m"  # grey
  warning = "\x1b[93;20m"  # yellow
  error = "\x1b[33;20m"  # orange
  critical = "\x1b[31;20m"  # red
  reset = "\x1b[0m"

  ch_format = '%(name)-11s: [%(levelname)-8s] %(message)s'
  FORMATS = {
    logging.DEBUG: debug + ch_format + reset,
    logging.INFO: informational + ch_format + reset,
    logging.WARNING: warning + ch_format + reset,
    logging.ERROR: error + ch_format + reset,
    logging.CRITICAL: critical + ch_format + reset
  }

  def format(self, record):
    log_fmt = self.FORMATS.get(record.levelno)
    formatter = logging.Formatter(log_fmt)
    return formatter.format(record)


def setup_logging(log_dir: str, debug: bool = False) -> None:
  datentime = datetime.now().strftime("%Y%m%d_%H")
  # logger = logging.getLogger('pynutbutter')

  log_file = Path(f"{log_dir}/pynutbutter_{datentime}.log")

  if not os.path.isfile(log_file):
    if os.name == 'nt':
      try:
        open(log_file, 'wb').close()
      except Exception as e:
        print(f"Unable to create log file at {log_file}: {e}")
        exit(1)
    else:
      try:
        os.mknod(log_file, 0o644 | stat.S_IRUSR)
      except Exception as e:
        print(f"Unable to create log file at {log_file}: {e}")
        exit(1)

  logger = logging.getLogger('pynutbutter')
  logger.setLevel(logging.DEBUG)

  ch = logging.StreamHandler()
  fh = logging.FileHandler(log_file)

  if not debug:
    ch.setLevel(logging.WARNING)
    fh.setLevel(logging.INFO)
  else:
    ch.setLevel(logging.DEBUG)
    fh.setLevel(logging.DEBUG)

  fh_format = logging.Formatter(
    fmt='%(asctime)s %(name)-11s [%(levelname)-8s] %(message)s',
    datefmt='%Y%m%d %H:%M:%S%z',
  )

  ch.setFormatter(colorized_logging_formatter())
  fh.setFormatter(fh_format)

  logger.addHandler(ch)
  logger.addHandler(fh)

  logger.debug("Logger subsystem initalized.")


def logg(severity: str = "info", message: str = ""):
  """Generates a syslog message.

  Args:
    severity (str, optional): Log level. Defaults to "info".
      Also accepts "debug", "exception", "critical", "warning", and "error".
      "exception" is special: it will append a stack trace to the log.
    message (str, optional): The log message. Defaults to "".
  """
  logger = logging.getLogger('pynutbutter')
  if match('d', severity, IGNORECASE):
    logger.debug(message)
  elif match('ex', severity, IGNORECASE):
    logger.exception(message)
    logging.shutdown()
  elif match('c', severity, IGNORECASE):
    logger.critical(message)
    logging.shutdown()
  elif match('w', severity, IGNORECASE):
    logger.warn(message)
  elif match('e', severity, IGNORECASE):
    logger.error(message)
  else:
    logger.info(message)


# Convienence Accessors


def debugging() -> bool:
  return config.getboolean('DEFAULT', 'debug', fallback=False)


def apikey() -> str:
  return config.get('DEFAULT', 'apikey')


def apiemail() -> str:
  return config.get('DEFAULT', 'apiemail')


def transaction_timeout() -> float:
  return config.getfloat('DEFAULT', 'timeout', fallback=5.0)


def max_retries() -> int:
  return config.getint('DEFAULT', 'max_retries', fallback=3)


def backoff() -> float:
  return config.getfloat('DEFAULT', 'backoff', fallback=2.0)


def daily_cal() -> int:
  return config.getint('DEFAULT', 'daily_cal', fallback=2000)


def daily_values():
  return dv_nutrients