import logging
from _typeshed import Incomplete

log_dir: Incomplete
user_cache: Incomplete
config_file: Incomplete
cache_file: Incomplete
debug: bool
config: Incomplete


def load_configuration() -> None:
  ...


class colorized_logging_formatter(logging.Formatter):
  debug: str
  informational: str
  warning: str
  error: str
  critical: str
  reset: str
  ch_format: str
  FORMATS: Incomplete

  def format(self, record):
    ...


def setup_logging(log_dir: str, debug: bool = ...) -> None:
  ...


def logg(severity: str = ..., message: str = ...):
  ...


def debugging() -> bool:
  ...


def apikey() -> str:
  ...


def apiemail() -> str:
  ...


def transaction_timeout() -> float:
  ...


def max_retries() -> int:
  ...


def backoff() -> float:
  ...
