from _typeshed import Incomplete
from distutils.log import debug as debug
from json import JSONEncoder
from pprint import pprint as pprint
from pynutbutter.fdc_enums import fdc_data_type as fdc_data_type, fdc_nutrients as fdc_nutrients
from pynutbutter.pynutbutter import config as config
from textwrap import indent as indent
from typing import List, Optional, Union


def is_in_nutrients(nutrient_num: str) -> bool:
  ...


class dataclass_formatter(JSONEncoder):

  def default(self, o):
    ...


class Nutrient:
  number: str
  name: str
  unit: str
  id: Optional[int]
  amount: Optional[float]
  rank: Optional[int]
  derivation_code: Optional[str]
  derivation_description: Optional[str]
  dv: Optional[float]

  def json(self) -> str:
    ...

  def set_dv(self, inbound_value: str, inbound_measure: str):
    ...

  def __init__(
    self,
    number,
    name,
    unit,
    id,
    amount,
    rank,
    derivation_code,
    derivation_description,
    dv
  ) -> None:
    ...


class NutrientConversionFactor:
  type: str
  value: float

  def __init__(self, type, value) -> None:
    ...


class FoodComponent:
  id: int
  name: str
  weight: float
  refuse: bool
  data_points: int
  year_acquired: int
  percent_weight: float

  def __init__(
    self, id, name, weight, refuse, data_points, year_acquired, percent_weight
  ) -> None:
    ...


class FoodCategory:
  id: int
  code: str
  description: str

  def __init__(self, id, code, description) -> None:
    ...


class MeasureUnit:
  abbr: str
  id: Optional[int]
  name: Optional[str]

  def __init__(self, abbr, id, name) -> None:
    ...


class FoodPortion:
  amount: float
  measure_unit: MeasureUnit
  data_points: Optional[int]
  description: Optional[str]
  id: Optional[int]
  serving_size: Optional[str]
  sort_num: Optional[int]
  weight: Optional[float]
  year_acquired: Optional[int]

  def __init__(
    self,
    amount,
    measure_unit,
    data_points,
    description,
    id,
    serving_size,
    sort_num,
    weight,
    year_acquired
  ) -> None:
    ...


class SampleFood:
  fdc_id: int
  data_type: str
  description: str
  food_class: str
  pub_date: str
  attributes: List[FoodCategory]

  def __init__(
    self, fdc_id, data_type, description, food_class, pub_date, attributes
  ) -> None:
    ...


class FoundationInputFoods:
  id: int
  description: str
  food: SampleFood

  def __init__(self, id, description, food) -> None:
    ...


class FoodAttributeType:
  id: int
  name: str
  description: str

  def __init__(self, id, name, description) -> None:
    ...


class FoodAttribute:
  id: int
  sequence: int
  value: str
  attribute_type: FoodAttributeType

  def __init__(self, id, sequence, value, attribute_type) -> None:
    ...


class RetentionFactor:
  id: int
  code: int
  description: str

  def __init__(self, id, code, description) -> None:
    ...


class SurveyInputFood:
  id: int
  amount: float
  code: int
  description: str
  weight: float
  portion_code: str
  portion_description: str
  sequence: int
  survey_flag: int
  unit: str

  def __init__(
    self,
    id,
    amount,
    code,
    description,
    weight,
    portion_code,
    portion_description,
    sequence,
    survey_flag,
    unit
  ) -> None:
    ...


class WweiaFoodCategory:
  code: int
  description: str

  def __init__(self, code, description) -> None:
    ...


class Food:
  fdc_id: int
  description: str
  data_type: fdc_data_type
  attributes: Optional[Union[str, List[FoodCategory], List[FoodAttribute]]]
  available_date: Optional[str]
  brand: Optional[str]
  food_class: Optional[str]
  code: Optional[str]
  components: Optional[List[FoodComponent]]
  data_source: Optional[str]
  footnotes: Optional[str]
  historical: Optional[bool]
  household_serving_size: Optional[str]
  ingredients: Optional[str]
  input_foods: Optional[Union[List[FoundationInputFoods],
                              List[SurveyInputFood]]]
  label_nutrients: Optional[dict]
  ndb_id: Optional[str]
  nutrient_conversion_factors: Optional[List[NutrientConversionFactor]]
  nutrients: Optional[List[Nutrient]]
  portions: Optional[List[FoodPortion]]
  pub_date: Optional[str]
  scientific_name: Optional[str]
  serving_size: Optional[int]
  serving_unit: Optional[str]
  survey_end: Optional[str]
  survey_start: Optional[str]
  upc: Optional[str]
  update_log: Optional[List[str]]
  wweia_food_category: Optional[WweiaFoodCategory]

  def add_attributes(
    self, attr: Union[List[FoodCategory], List[FoodAttribute]]
  ) -> None:
    ...

  def add_available_date(self, date: str) -> None:
    ...

  def add_brand(self, brand: str) -> None:
    ...

  def add_food_class(self, food_class: str) -> None:
    ...

  def add_food_code(self, code: str) -> None:
    ...

  def add_components(self, comps: List[FoodComponent]) -> None:
    ...

  def add_data_source(self, source: str) -> None:
    ...

  def add_footnotes(self, notes: str) -> None:
    ...

  is_historical: Incomplete

  def flag_historical(self, is_historical: bool) -> None:
    ...

  def add_household_serving_size(self, size: str) -> None:
    ...

  def add_ingredients(self, ingredients: str) -> None:
    ...

  def add_input_foods(
    self, inputs: Union[List[FoundationInputFoods], List[SurveyInputFood]]
  ) -> None:
    ...

  def add_label_nutrients(self, label: dict) -> None:
    ...

  def add_ndb_id(self, id: str) -> None:
    ...

  def add_nutrient_conversion_factors(
    self, ncf: List[NutrientConversionFactor]
  ) -> None:
    ...

  def add_nutrients(self, nutrients: List[Nutrient]) -> None:
    ...

  def add_portions(self, portions: List[FoodPortion]) -> None:
    ...

  def add_pub_date(self, date: str) -> None:
    ...

  def add_scientific_name(self, name: str) -> None:
    ...

  def add_serving_size(self, size: int) -> None:
    ...

  def add_serving_unit(self, unit: str) -> None:
    ...

  def add_survey_end(self, end: str) -> None:
    ...

  def add_survey_start(self, start: str) -> None:
    ...

  def add_upc(self, upc: str) -> None:
    ...

  def add_update_log(self, logs: List[str]) -> None:
    ...

  def add_wweia(self, category) -> None:
    ...

  def json(self) -> str:
    ...

  def abridged_nutrients(self, daily_cal: int = ...) -> str:
    ...

  def abridged(self) -> str:
    ...

  def __init__(
    self,
    fdc_id,
    description,
    data_type,
    attributes,
    available_date,
    brand,
    food_class,
    code,
    components,
    data_source,
    footnotes,
    historical,
    household_serving_size,
    ingredients,
    input_foods,
    label_nutrients,
    ndb_id,
    nutrient_conversion_factors,
    nutrients,
    portions,
    pub_date,
    scientific_name,
    serving_size,
    serving_unit,
    survey_end,
    survey_start,
    upc,
    update_log,
    wweia_food_category
  ) -> None:
    ...
