#!/usr/bin/env python

from dataclasses import asdict, dataclass, field, is_dataclass
from json import JSONEncoder, dumps
from typing import List, Optional, Union

from measurement.measures import Weight
from measurement.utils import guess

from pynutbutter.fdc_enums import fdc_data_type, fdc_nutrients as fdcn
from pynutbutter.pynutbutter import config, daily_cal, daily_values


def is_in_nutrients(nutrient_num: str) -> bool:
  try:
    return digest.dv(nutrient_num)
  except ValueError:
    return False

  return True


class dataclass_formatter(JSONEncoder):

  def default(self, o):
    if is_dataclass(o):
      return asdict(o)
    return super().default(o)


@dataclass
class Nutrient:
  number: str
  name: str
  unit: str
  # TODO: Do we need the ID? It doesn't look like it...
  id: Optional[int] = None
  amount: Optional[float] = None
  rank: Optional[int] = None
  derivation_code: Optional[str] = None
  derivation_description: Optional[str] = None
  dv: Optional[float] = None

  def json(self) -> str:
    return dumps(vars(self))

  def set_dv(self, inbound_value: str, inbound_measure: str):
    # TODO: Fix this function to work with the new JSON setup!
    # if is_in_nutrients(self.number):
    #   dv_value = float(dv_value(self.number).value)
    #   dv_measure = dv_value(self.number).value['measure']
    #   dvo = daily_cal()

    #   dv_measure = guess(dv_value, dv_measure, measures=[Weight])
    #   inbound_measure = guess(
    #     inbound_value, inbound_measure, measures=[Weight]
    #   ).dv_measure

    #   self.dv = (inbound_measure / (dv_measure * dvo)) * 100
    # else:
    self.dv = None


@dataclass
class NutrientConversionFactor:
  type: str
  value: float


@dataclass
class FoodComponent:
  id: int
  name: str
  weight: float
  refuse: bool
  data_points: int
  year_acquired: int
  percent_weight: float


@dataclass
class FoodCategory:
  id: int
  code: str
  description: str


@dataclass
class MeasureUnit:
  abbr: str
  id: Optional[int] = None
  name: Optional[str] = None


@dataclass
class FoodPortion:
  amount: float
  measure_unit: MeasureUnit
  data_points: Optional[int] = None
  description: Optional[str] = None
  id: Optional[int] = None
  serving_size: Optional[str] = None
  sort_num: Optional[int] = None
  weight: Optional[float] = None
  year_acquired: Optional[int] = None


@dataclass
class SampleFood:
  fdc_id: int
  data_type: str
  description: str
  food_class: str
  pub_date: str
  attributes: List[FoodCategory] = field(default_factory=list)


@dataclass
class FoundationInputFoods:
  id: int
  description: str
  food: SampleFood


@dataclass
class FoodAttributeType:
  id: int
  name: str
  description: str


@dataclass
class FoodAttribute:
  id: int
  sequence: int
  value: str
  attribute_type: FoodAttributeType


@dataclass
class RetentionFactor:
  id: int
  code: int
  description: str


# TODO: Update to match FDC API. We don't support recursive InputFoods yet
@dataclass
class SurveyInputFood:
  id: int
  amount: float
  code: int
  description: str
  weight: float
  portion_code: str
  portion_description: str
  sequence: int
  survey_flag: int
  unit: str


@dataclass
class WweiaFoodCategory:
  code: int
  description: str


@dataclass
class Food:
  fdc_id: int
  description: str
  data_type: fdc_data_type
  attributes: Optional[Union[str, List[FoodCategory],
                             List[FoodAttribute]]] = None
  available_date: Optional[str] = None
  brand: Optional[str] = None
  food_class: Optional[str] = None
  code: Optional[str] = None
  components: Optional[List[FoodComponent]] = None
  data_source: Optional[str] = None
  footnotes: Optional[str] = None
  historical: Optional[bool] = None
  household_serving_size: Optional[str] = None
  ingredients: Optional[str] = None
  input_foods: Optional[Union[List[FoundationInputFoods],
                              List[SurveyInputFood]]] = None
  label_nutrients: Optional[dict] = None
  ndb_id: Optional[str] = None
  nutrient_conversion_factors: Optional[List[NutrientConversionFactor]] = None
  nutrients: Optional[List[Nutrient]] = None
  portions: Optional[List[FoodPortion]] = None
  pub_date: Optional[str] = None
  scientific_name: Optional[str] = None
  serving_size: Optional[int] = None
  serving_unit: Optional[str] = None
  survey_end: Optional[str] = None
  survey_start: Optional[str] = None
  upc: Optional[str] = None
  update_log: Optional[List[str]] = None
  wweia_food_category: Optional[WweiaFoodCategory] = None

  def add_attributes(
    self, attr: Union[List[FoodCategory], List[FoodAttribute]]
  ) -> None:
    self.attributes = attr

  def add_available_date(self, date: str) -> None:
    self.available_date = date

  def add_brand(self, brand: str) -> None:
    self.brand = brand

  def add_food_class(self, food_class: str) -> None:
    self.food_class = food_class

  def add_food_code(self, code: str) -> None:
    self.code = code

  def add_components(self, comps: List[FoodComponent]) -> None:
    self.components = comps

  def add_data_source(self, source: str) -> None:
    self.data_source = source

  def add_footnotes(self, notes: str) -> None:
    self.footnotes = notes

  def flag_historical(self, is_historical: bool) -> None:
    self.is_historical = is_historical

  def add_household_serving_size(self, size: str) -> None:
    self.household_serving_size = size

  def add_ingredients(self, ingredients: str) -> None:
    self.ingredients = ingredients

  def add_input_foods(
    self, inputs: Union[List[FoundationInputFoods], List[SurveyInputFood]]
  ) -> None:
    self.input_foods = inputs

  def add_label_nutrients(self, label: dict) -> None:
    self.label_nutrients = label

  def add_ndb_id(self, id: str) -> None:
    self.ndb_id = id

  def add_nutrient_conversion_factors(
    self, ncf: List[NutrientConversionFactor]
  ) -> None:
    self.nutrient_conversion_factors = ncf

  def add_nutrients(self, nutrients: List[Nutrient]) -> None:
    self.nutrients = nutrients

  def add_portions(self, portions: List[FoodPortion]) -> None:
    self.portions = portions

  def add_pub_date(self, date: str) -> None:
    self.pub_date = date

  def add_scientific_name(self, name: str) -> None:
    self.scientific_name = name

  def add_serving_size(self, size: int) -> None:
    self.serving_size = size

  def add_serving_unit(self, unit: str) -> None:
    self.serving_unit = unit

  def add_survey_end(self, end: str) -> None:
    self.survey_end = end

  def add_survey_start(self, start: str) -> None:
    self.survey_start = start

  def add_upc(self, upc: str) -> None:
    self.upc = upc

  def add_update_log(self, logs: List[str]) -> None:
    self.update_log = logs

  def add_wweia(self, category) -> None:
    self.wweia_food_category = category

  def json(self) -> str:
    return dumps(self.__dict__, cls=dataclass_formatter, indent=2)

  def abridged_nutrients(self) -> str:
    caloric_offset = daily_cal() / 2000

    total_fats = "Total Fats: 0.00g (0%)"
    sat_fats = "  Sat. Fats: 0.00g (0%)"
    trans_fats = "  Trans Fats: 0.00g (0%)"
    polyunsat_fats = "  Polyunsat. Fats: 0.00g (0%)"
    monounsat_fats = "  Monounsat. Fats: 0.00g (0%)"
    cholesterol = "Cholesterol: 0.00g (0%)"
    sodium = "Sodium: 0.00mg (0%)"
    carbohydrates = "Carbohydrates: 0.00g (0%)"
    fiber = "Fiber: 0.00g (0%)"
    total_sugars = "Total Sugars: 0.00g (0%)"
    vit_d = "Vitamin D: 0.00mg (0%)"
    calcium = "Calcium: 0.00g (0%)"
    iron = "Iron: 0.00mg (0%)"
    potassium = "Potassium: 0.00mg (0%)"

    ## TODO: Update the below values to work since we've consolidated everything
    ##       to milligrams!!

    for nu in self.nutrients:
      num = float(nu.number)
      n = [f"{nu.amount:2.2f}", nu.unit, nu.dv]

      # TODO: Need to log here.
      # Skip unabridged nutrients
      try:
        fdcn(num).name
      except ValueError:
        continue

      if n[2] is not None:
        n[2] = f" ({n[2]:2f}%)"

      if num == float(fdcn.FAT.value) \
         or num == float(fdcn.FAT_TOTAL.value):

        total_fats = f"Total Fats: {n[0]} {n[1]}"
        if n[2]:
          total_fats += n[2]
        continue

      if num == float(fdcn.SATURATED_FAT.value):

        sat_fats = f"  Sat. Fats: {n[0]} {n[1]}"
        if n[2]:
          sat_fats += n[2]
        continue

      if num == float(fdcn.TRANS_FAT.value):
        trans_fats = f"  Trans Fats: {n[0]} {n[1]}"
        if n[2]:
          trans_fats += n[2]
        continue

      if num == float(fdcn.POLYUNSATURATED_FAT.value):
        polyunsat_fats = f"  Polyunsaturated Fats: {n[0]} {n[1]}"
        if n[2]:
          polyunsat_fats += n[2]
        continue

      if num == float(fdcn.MONOUNSATURATED_FAT.value):
        monounsat_fats = f"  Monounsaturated Fats: {n[0]} {n[1]}"
        if n[2]:
          monounsat_fats += n[2]
        continue

      if num == float(fdcn.CHOLESTEROL.value):
        if n[1] == 'mg' and float(n[0]) > 1000.0:
          cholesterol = f"Cholesterol: {float(n[0])/1000:2.2f} g"
        else:
          cholesterol = f"Cholesterol: {n[0]} {n[1]}"

        if n[2]:
          cholesterol += n[2]
        continue

      if num == float(fdcn.SODIUM.value):
        if n[1] == 'mg' and float(n[0]) > 1000.0:
          sodium = f"Sodium: {float(n[0])/1000:2.2f} g"
        else:
          sodium = f"Sodium: {n[0]} {n[1]}"

        if n[2]:
          sodium += n[2]
        continue

      if num == float(fdcn.CARBOHYDRATE.value):
        carbohydrates = f"Carbohydrates: {n[0]} {n[1]}"
        if n[2]:
          carbohydrates += n[2]
        continue

      if num == float(fdcn.FIBER.value):
        fiber = f"Fiber: {n[0]} {n[1]}"
        if n[2]:
          fiber += n[2]
        continue

      if num == float(fdcn.SUGAR.value) or num == float(fdcn.SUGARS.value):
        total_sugars = f"Total Sugars: {n[0]} {n[1]}"
        if n[2]:
          total_sugars += n[2]
        continue

    # #TODO: Make sure fdcn.SUGAR and fdcn.SUGARS are the same,
    # #      and not added vs regularly-occuring sugars, respectively.

    # # TODO: Find the nutrient number for added sugars.
    # # try:
    # #   added_sugars = [f"{n.amount} {n.unit}" for n in self.nutrients
    # #                   if float(n.number) == fdcn.SUGAR.value][0]
    # # except IndexError:
    # #   added_sugars = None

      if num == float(fdcn.VIT_D.value):
        vit_d = f"Vitamin D: {n[0]} {n[1]}"
        if n[2]:
          vit_d += n[2]
        continue

      if num == float(fdcn.CALCIUM.value):
        calcium = f"Calcium : {n[0]} {n[1]}"
        if n[2]:
          calcium += n[2]
        continue

      if num == float(fdcn.IRON.value):
        iron = f"Iron : {n[0]} {n[1]}"
        if n[2]:
          iron += n[2]
        continue

      if num == float(fdcn.POTASSIUM.value):
        potassium = f"Potassium : {n[0]} {n[1]}"
        if n[2]:
          potassium += n[2]
        continue

    liner = '=' * max(len(polyunsat_fats), len(monounsat_fats))

    return '\n'.join([
      liner,
      total_fats,
      sat_fats,
      trans_fats,
      polyunsat_fats,
      monounsat_fats,
      cholesterol,
      sodium,
      carbohydrates,
      fiber,
      total_sugars,
      liner,
      vit_d,
      calcium,
      iron,
      potassium
    ])

  def abridged(self) -> str:
    content_lines = []

    content_lines.append(f"Food: {self.description} (FDCID: {self.fdc_id})")

    if self.data_type == fdc_data_type.BRANDED:
      content_lines.append(
        '\n'.join([
          f"Publish Date: {self.pub_date}",
          f"Brand: {self.brand}",
          f"UPC: {self.upc}",
          f"Serving Size: {self.serving_size} {self.serving_unit}",
        ])
      )
    elif self.data_type == fdc_data_type.FOUNDATION:
      content_lines.append(f"Publication Date: {self.pub_date}")
      if self.attributes:
        content_lines.append(f"Category:         {self.attributes}")

    if self.scientific_name is not None:
      content_lines.append(f"Scientific Name:  {self.scientific_name}")
    elif self.data_type == fdc_data_type.LEGACY:
      pass
    elif self.data_type == fdc_data_type.SURVEY:
      pass

    content_lines.append(self.abridged_nutrients())

    return '\n'.join(content_lines)

  #     fdc_id: int
  # description: str
  # data_type: fdc_data_type
  # attributes: Optional[Union[str, List[FoodCategory],
  #                            List[FoodAttribute]]] = None
  # available_date: Optional[str] = None
  # brand: Optional[str] = None
  # food_class: Optional[str] = None
  # code: Optional[str] = None
  # components: Optional[List[FoodComponent]] = None
  # data_source: Optional[str] = None
  # footnotes: Optional[str] = None
  # historical: Optional[bool] = None
  # household_serving_size: Optional[str] = None
  # ingredients: Optional[str] = None
  # input_foods: Optional[Union[List[FoundationInputFoods],
  #                             List[SurveyInputFood]]] = None
  # label_nutrients: Optional[dict] = None
  # ndb_id: Optional[str] = None
  # nutrient_conversion_factors: Optional[List[NutrientConversionFactor]] = None
  # nutrients: Optional[List[Nutrient]] = None
  # portions: Optional[List[FoodPortion]] = None
  # pub_date: Optional[str] = None
  # scientific_name: Optional[str] = None
  # serving_size: Optional[int] = None
  # serving_unit: Optional[str] = None
  # survey_end: Optional[str] = None
  # survey_start: Optional[str] = None
  # upc: Optional[str] = None
  # update_log: Optional[List[str]] = None
  # wweia_food_category: Optional[WweiaFoodCategory] = None
