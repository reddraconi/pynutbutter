#!/usr/bin/env python

from pynutbutter.fdc_api import query

from pynutbutter.fdc_enums import fdc_data_format, fdc_data_type

# Ask USDA FDC to return the first to results for 'Cheddar Cheese'
cheese = query(
  query="cheddar cheese",
  max_items=1,
  format=fdc_data_format.FULL,
  data_type=fdc_data_type.BRANDED,
)

print(f"Got {len(cheese)} results!")

for c in cheese:
  #  print("JSON FORMAT:")
  #  print("-"*80)
  #  print(c.json())
  #  print("-"*80)
  #  print("ABRIDGED ASCII FORMAT:")
  #  print("-"*80)
  print(c.abridged())
  print("-" * 80)
#  print("NATIVE PYTHON OBJECT")
#  print("-"*80)
#  print(c)
