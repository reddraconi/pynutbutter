from .fdc_data_objects import (
  Food as Food,
  FoodAttribute as FoodAttribute,
  FoodAttributeType as FoodAttributeType,
  FoodCategory as FoodCategory,
  FoodComponent as FoodComponent,
  FoodPortion as FoodPortion,
  MeasureUnit as MeasureUnit,
  Nutrient as Nutrient,
  NutrientConversionFactor as NutrientConversionFactor,
  SurveyInputFood as SurveyInputFood,
  WweiaFoodCategory as WweiaFoodCategory
)
from .fdc_enums import fdc_data_type as fdc_data_type
from .pynutbutter import config as config, debugging as debugging, logg as logg
from measurement.utils import guess as guess
from requests import Response
from typing import Any


def ensure_val(dictionary: dict, key: str, default: Any) -> Any:
  ...


def chew(r: Response, **kwargs) -> list[Food]:
  ...
