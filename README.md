# pynutbutter

A greenhorn's attempt at creating a pypi-able package.

My aim is to do the following:

1. Create a v2-compliant API for the USDA Food Data Central (FDC) API
2. Get it working with GitLab's CI/CD pipelines
3. Make GitLab push it automatically to pypi testing and official
4. Hope someone finds it helpful

This code, as of version **0.1.3** is **UNSTABLE** and not approved
(or recommended) for real-world use.

## Usage (Quick Version)

1. You have to sign up for a (free!) API key before you can use this module.
   Visit <https://fdc.nal.usda.gov/api-key-signup.html> to sign up.

2. Assuming you're running Linux, run the following bash code. If you're on
   Windows or Mac OS, the path and even commands will differ.

   1. Replace `$USER` with your username.
   2. Replace `$API_KEY` with the API key you receive from the sign-up page
   3. Replace `$API_EMAIL` with the email you used to sign up for the API key
   4. Add a path for `log_file`, if desired. If not, it'll default to
      `/home/$USER/.cache/pynutbutter/`.

      ```bash
      mkdir /home/$USER/.config/pynutbutter/
      cat <<EOF > /home/$USER/.config/pynutbutter/config.ini
      [DEFAULT]
      apikey = $API_KEY
      apiemail = $API_EMAIL
      debug = false
      log_file =
      cache_file =
      timeout = 5.0
      max_retries = 3
      backoff = 2
      EOF
      ```

3. Install the `pynutbutter` package with pip

   ```bash
   python -m pip install pynutbutter
   ```

4. Check the module is installed

   ```bash
   python -m pynutbutter

   #Output:
   Pynutbutter ver 0.1.3
   ```

5. Use the new module in a script!

   ```python
   #!/usr/bin/env python

   from pynutbutter.fdc_api import query

   # Ask USDA FDC to return the first to results for 'Cheddar Cheese'
   cheese = query("Cheddar Cheese", max_results=2)
   print(cheese)
   ```

## Usage (Long Version)

### Prerequisites

The very first requirement, other that having at least python 3.7 installed on
your system, is to sign up for a free API key from
<https://fdc.nal.usda.gov/api-key-signup.html>. You'll need the key you recieve,
along with the email address you used to sign up, to use this module.

`pynutbutter` looks for a configuration file in XDG-compliant locations using
a dependency `appdirs`.

> Be sure to replace **`$USER`** with your user's name below.

#### Linux Users

We expect the following folders to be present for normal users.

- `/home/$USER/.config/`
- `/home/$USER/.local/`
- `/home/$USER/.cache/`

If instead, you would like to configure a site-wide (e.g., server-wide)
configuration, the following folders should be available.

- `/usr/local/etc`
- `/usr/local/share`

`pynutbutter` will look for `config.ini` in `/usr/local/etc/pynutbutter/` and
`/home/$USER/.config/pynutbutter/` in the listed order. The last one that is
found wins. This enables users to have specific overrides.

Logs are written to `/home/$USER/.local/pynutbutter/` unless the `log_file`
configuration item is modified in `config.ini`.

API queries are cached into a sqlite database and stored in
`/home/$USER/.cache/pynutbutter/pynutbutter.sqlite` unless `cache_file`
configuration item is modified in `config.ini`.

#### Windows Users

> This module has not _and probably won't be_ tested for Windows compatibility.
> We welcome patches if any issues arise. Please see `Submission Guidelines`
> below for more information.

We expect the following folder to be present for normal users.

- `C:\Users\$USER\AppData\Local\`

If instead, you are on an enterprise system with _Roaming Profiles_, the
following will need to be present:

- `C:\Users\$USER\AppData\Roaming\`

`pynutbutter` will look for `config.ini` in
`C:\Users\$USER\AppData\Roaming\pynutbutter` and
`C:\Users\$USER\AppData\Local\pynutbutter` in the listed order.
The last one that is found wins.

Logs are written to
`C:\Users\$user\AppData\Local\pynutbutter\Logs\*.log` unless the
`log_file` configuration item is modified in `config.ini`.

API queries are cached into a sqlite database and stored in
`C:\Users\$USER\AppData\Local\pynutbutter\Cache\pynutbutter.sqlite` unless
`cache_file` configuration item is modified in `config.ini`.

**The file extension must end in `.sqlite`**

#### MacOS Users

> This module has not _and probably won't be_ tested for MacOS compatability.
> We welcome patches if any issues arrise. Please see `Submission Guidelines`
> below for more information.

We expect the following folders to be present for normal users.

- `/Users/$USER/Library/Application Support/`
- `/Users/$USER/Library/Caches/`
- `/Users/$USER/Library/Logs`

If instead, you would like to configure site-wide (e.g., server-wide)
configuration, the following folders should be available.

- `/Library/Application Support`

`pynutbutter` will look for `config.ini` in
`/Users/$USER/Library/Application Support/pynutbutter` and
`/Library/Application Support/pynutbutter/` in the listed order.
The last one that is found wins.

Logs are written to `/Users/$USER/Library/Logs/pynutbutter/*.log` unless the
`log_file` configuration item is modified in `config.ini`.

**This _must_ be a file path writable by the user running the module!**

API queries are cached into a sqlite database and stored in
`/Users/$USER/Library/Caches/pynutbutter/pynutbutter.sqlite` unless
`cache_file` configuration item is modified in `config.ini`.

**The file extension should end in `.sqlite` and _must_ be in a file path
writable by the user running the module!**

### `pynutbutter` Configuration

After you receive your API key from _usda.gov_, copy the following example
`config.ini` into your system's appropraite location. Make sure the file is
readable and writable by the user running this module. (e.g., `0660` or `0664`
permissions for Linux and MacOS).

Replace **`$API_KEY`** with the API key you recieved from _usda.gov_ and
**`$API_EMAIL`** with the email address you used to sign up for the API key.

The email address is appended to all queries sent to the FDC SOAP API to help
them warn people that are abusing their service.

Both `log_file` and `cache_file` are blank by default. They'll get populated by
the module after the first run. You can manually input entries here, if desired,
to write them to custom locations.

If you do, use a file path for `log_file`. All log files created in that folder
will use the format `pynutbutter_<launch date>_<launch time>.log`. The
`cache_file` is a `.sqlite` file and must use that extension, wherever you write
it and whatever name you give it.

After the first launch, the comments below (if you copied them) won't be
present.

```ini
[DEFAULT]
# Key provided by https://fdc.nal.usda.gov/api-key-signup.html
apikey = $API_KEY
# Email used to sign up for API access
apiemail = $API_EMAIL
# Run pynutbutter in debug mode?
debug = false
# Example: log_file = /tmp/
log_file =
# Example: cache_file = /tmp/pynutbutter.sqlite
cache_file =
# Timeout in seconds for API calls
timeout = 5.0
# Maximum retries for API calls
max_retries = 3
# Exponential backoff timer in seconds for API calls
backoff = 2
# Override for daily recommened calories (2000 is the FDA-recommended maximum)
daily_cal = 2000
```

### Install `pynutbutter`

It's recommended to set up a virtual environment before you install
`pynutbutter`. We don't expect it to cause problems on your system or python
environment, but it's always better to be safe, rather than sorry.

The following examples are written for Linux (tested on Fedora 32+ and
Fedora 36).
If you're using Windows or MacOS, your commands may be different.

```bash
python -m venv .venv
source .venv/bin/activate
python -m pip install pynutbutter
```

### Use `pynutbutter` In a Script

If your configuration is accessible and correct, the module should start without
any issues. You can test this by running the following code in your favorite
terminal:

```bash
python -m pynutbutter
```

You should get `Pynutbutter ver 0.1.3`.

This is a quick script example:

```python
#!/usr/bin/env python

from pynutbutter.fdc_api import query
from pynutbutter.fdc_enums import fdc_data_format, fdc_data_type

# Ask USDA FDC to return the first result for 'Cheddar Cheese'
cheese = query(
  query="cheddar cheese",
  max_results=1,
  format=fdc_data_format.FULL,
  data_type=fdc_data_type.BRANDED
)

print(cheese[0].json())
```

If you get a JSON string back, then everything's working!

```text
{
  "fdc_id": 2015943,
  "description": "CHEDDAR CHEESE",
  "data_type": "Branded",
  "attributes": null,
  "available_date": null,
  "brand": "Three Square Inc.'s Crystal Farms",
  "food_class": null,
  "code": null,
  "components": null,
  "data_source": "LI",
  "footnotes": null,
  "historical": null,
  "household_serving_size": null,
  "ingredients": "Cheddar Cheese..."
<output omitted>
```

> **Note:** This module also supports an abridged (ASCII) output, as well as
> access to the native Python pynutbutter.Food object.

## FDC API

`Pynutbutter` currenly supports the following API objects, but maybe not in the
way you'd expect. (We consolidated some things to make long-term use easier).

- Abridged, Branded, Foundation, SRLegacy, and Sample Food Items
  - These are all consolidated into one `Food` object
- Nutrient and Food Nutrient Items (including derivations)
  - These are consolidated into one `Nutrient` object

> If you need more information on the FDC API itself, please visit
> [USDA Food Data Central API Guide][usda_fdc_api]
> and [USDA FDC API at Swaggerhub][usda_swag]

[usda_fdc_api]: https://fdc.nal.usda.gov/api-guide.html
[usda_swag]: https://app.swaggerhub.com/apis/fdcnal/food-data_central_api/1.0.0

## Submission Guidelines

We're still relatively new to writing Python stuff for public consumption, so
any supportive feedback is greatly appreciated! Issues can be posted to our
[Issues](https://gitlab.com/reddraconi/pynutbutter/-/issues) page at GitLab.

To format pull requests use `yapf`, along with the `.style.yapf` file in the
root of this repo.

We also require running `mypy` and `bandit` against the code in
`src/pynutbutter`, to make sure everything is relatively healthy. If High or
Medium issues are returned by `bandit` run against code in a PR, it will be
rejected.
