#!/usr/bin/env python

from pynutbutter.fdc_api import query as que, fdc_data_type as fdt
import json

results = que(query="Cheddar Cheese", data_type=fdt.BRANDED.value, total_hits=1)

for res in results:
  print("Abridged Output Example:")
  print(res.abridged())

# results = que(query="Cheddar Cheese", type=fdt.BRANDED.value, total_hits=1)

# for res in results:
#   print("JSON Output Example:")
#   print(json.dumps(res.json(), indent=2))